use tombstone::usecase::elliptic_curve_usecase::extended_euclidean_algorithm;
use tombstone::usecase::elliptic_curve_usecase::EllipticCurveUsecase;

#[test]
pub fn test_extended_euclidean_algorithm() {
    // Ensure gcd is calculated correctly.
    assert_eq!(extended_euclidean_algorithm(3 * 3, 4 * 5).0, 1);
    assert_eq!(extended_euclidean_algorithm(2 * 5, 3 * 5).0, 5);
    assert_eq!(
        extended_euclidean_algorithm(2 * 3 * 4 * 5 * 6, 2 * 4 * 6).0,
        2 * 4 * 6
    );

    // Ensure x and y of Bezout's identity are calculated correctly.
    assert_eq!(extended_euclidean_algorithm(99, 78).1, -11);
    assert_eq!(extended_euclidean_algorithm(99, 78).2, 14);
}

#[test]
pub fn test_add_points() {
    let usecase = EllipticCurveUsecase::new();
    let generator_point = usecase.create_point(5, 1);
    let curve = usecase.create_curve(17, 2, 2, generator_point);
    // let point1 = usecase.create_point(5, 1);
    // let point2 = usecase.create_point(5, 2);

    let point_sum1_expected = usecase.create_point(6, 3);
    let point_sum1_result = usecase.add_points(curve, generator_point, generator_point);
    assert_eq!(point_sum1_expected.equals(point_sum1_result), true);
}
