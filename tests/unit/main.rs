extern crate futures;
extern crate js_sys;
extern crate tombstone;
extern crate wasm_bindgen;
extern crate wasm_bindgen_futures;
extern crate wasm_bindgen_test;

pub mod usecase;
