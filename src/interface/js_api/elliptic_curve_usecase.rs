
use domain::elliptic_curve::EllipticCurve;
// use domain::elliptic_curve::EllipticCurvePoint;

pub trait EllipticCurveUsecase {
    fn create_curve (&self) -> EllipticCurve;
    // fn add_points (&self, curve: EllipticCurve, point_p: EllipticCurvePoint, point_q: EllipticCurvePoint) -> EllipticCurvePoint;
    // fn multiply_scalar (&self, curve: EllipticCurve, point: EllipticCurvePoint, k: i32) -> EllipticCurvePoint;
}
