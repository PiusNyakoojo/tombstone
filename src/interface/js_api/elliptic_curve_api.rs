use macros::log;
use wasm_bindgen::prelude::*;

// use super::elliptic_curve_usecase::EllipticCurveUsecase;
use domain::elliptic_curve::EllipticCurve;
use domain::elliptic_curve::EllipticCurvePoint;
use usecase::elliptic_curve_usecase::EllipticCurveUsecase;

#[wasm_bindgen]
#[derive(Copy, Clone)]
pub struct EllipticCurveApi {
    pub elliptic_curve_usecase: EllipticCurveUsecase,
}

#[wasm_bindgen]
impl EllipticCurveApi {
    #[wasm_bindgen(constructor)]
    pub fn new(ec_usecase: EllipticCurveUsecase) -> EllipticCurveApi {
        let api = EllipticCurveApi {
            elliptic_curve_usecase: ec_usecase,
        };
        return api;
    }
    pub fn create_curve(&self, p: i32, a: i32, b: i32, g: EllipticCurvePoint) -> EllipticCurve {
        return self.elliptic_curve_usecase.create_curve(p, a, b, g);
    }
    pub fn create_point(&self, x: i32, y: i32) -> EllipticCurvePoint {
        return self.elliptic_curve_usecase.create_point(x, y);
    }
    pub fn add_points(
        &self,
        curve: &EllipticCurve,
        point1: &EllipticCurvePoint,
        point2: &EllipticCurvePoint,
    ) -> EllipticCurvePoint {
        console_log!("Point1: {}!", point1.x);
        return self
            .elliptic_curve_usecase
            .add_points(*curve, *point1, *point2);
    }
}
