
pub mod elliptic_curve_api;
pub mod elliptic_curve_usecase;

use wasm_bindgen::prelude::*;
use self::elliptic_curve_api::EllipticCurveApi;

#[wasm_bindgen]
#[derive(Copy, Clone)]
pub struct JsApi {
    pub elliptic_curve_api: EllipticCurveApi
}

#[wasm_bindgen]
impl JsApi {
    #[wasm_bindgen(constructor)]
    pub fn new (ec_api: EllipticCurveApi) -> JsApi {
        let js_api = JsApi{
            elliptic_curve_api: ec_api
        };
        return js_api
    }
}