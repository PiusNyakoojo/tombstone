use macros::log;
use wasm_bindgen::prelude::*;

use domain::elliptic_curve::EllipticCurve;
use domain::elliptic_curve::EllipticCurvePoint;
// use interface::js_api::elliptic_curve_usecase;

#[wasm_bindgen]
#[derive(Copy, Clone)]
pub struct EllipticCurveUsecase {}

#[wasm_bindgen]
impl EllipticCurveUsecase {
    #[wasm_bindgen(constructor)]
    pub fn new() -> EllipticCurveUsecase {
        let ec_usecase = EllipticCurveUsecase {};
        ec_usecase
    }

    pub fn create_curve(&self, p: i32, a: i32, b: i32, g: EllipticCurvePoint) -> EllipticCurve {
        let i = EllipticCurvePoint { x: 0, y: 0 };
        let curve = EllipticCurve::new(p, a, b, g, i);
        curve
    }

    pub fn create_point(&self, x: i32, y: i32) -> EllipticCurvePoint {
        return EllipticCurvePoint { x: x, y: y };
    }

    pub fn add_points(
        &self,
        curve: EllipticCurve,
        point1: EllipticCurvePoint,
        point2: EllipticCurvePoint,
    ) -> EllipticCurvePoint {
        if point1.equals(curve.i_identity) {
            return point2;
        }

        if point2.equals(curve.i_identity) {
            return point1;
        }

        let (s_numerator, s_denominator) = if point1.equals(point2) {
            let n = (3 * point1.x * point1.x + curve.a_coefficient) % curve.p_prime;
            let d = extended_euclidean_algorithm(2 * point1.y, curve.p_prime).1 % curve.p_prime;

            if point1.y == 0 {
                return curve.i_identity;
            }
            (n, d)
        } else {
            console_log!("Before Numerator: {}", (point1.y - point2.y));
            let n = (point1.y - point2.y) % curve.p_prime;
            console_log!("Numerator: {}", n);
            let d =
                extended_euclidean_algorithm(point1.x - point2.x, curve.p_prime).1 % curve.p_prime;

            if point1.x == point2.x {
                return curve.i_identity;
            }
            (n, d)
        };

        let s = (s_numerator * s_denominator) % curve.p_prime;
        let result_x = (s * s - (point1.x + point2.x)) % curve.p_prime;
        let result_y = (s * (point1.x - result_x) - point1.y) % curve.p_prime;
        let result_point = EllipticCurvePoint::new(result_x, result_y);

        return result_point;
    }

    // Elliptic Curve Diffie Hellman - ECDH
    // pub fn generate_key_pair
    // pub fn encrypt_message
    // pub fn decrypt_message

    // Elliptic Curve Digital Signature - ECDSA
    // pub fn sign_message
    // pub fn verify_signature
}

pub fn extended_euclidean_algorithm(a: i32, b: i32) -> (i32, i32, i32) {
    if b == 0 {
        return (a, 1, 0);
    }

    let (gcd1, x1, y1) = extended_euclidean_algorithm(b, a % b);
    let gcd = gcd1;
    let x = y1;
    let y = x1 - (a / b) * y1;
    return (gcd, x, y);
}
