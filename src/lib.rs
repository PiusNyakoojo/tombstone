extern crate js_sys;
extern crate wasm_bindgen;
extern crate web_sys;

#[macro_use]
pub mod macros;

pub mod domain;
pub mod interface;
pub mod js_library;
pub mod provider;
pub mod rust_library;
pub mod usecase;
