use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Copy, Clone)]
pub struct EllipticCurve {
    pub p_prime: i32,
    pub a_coefficient: i32,
    pub b_coefficient: i32,
    pub g_generator: EllipticCurvePoint,
    pub i_identity: EllipticCurvePoint,
}

#[wasm_bindgen]
impl EllipticCurve {
    #[wasm_bindgen(constructor)]
    pub fn new(
        p: i32,
        a: i32,
        b: i32,
        g: EllipticCurvePoint,
        i: EllipticCurvePoint,
    ) -> EllipticCurve {
        let curve = EllipticCurve {
            p_prime: p,
            a_coefficient: a,
            b_coefficient: b,
            g_generator: g,
            i_identity: i,
        };
        curve
    }
}

#[wasm_bindgen]
#[derive(Copy, Clone)]
pub struct EllipticCurvePoint {
    pub x: i32,
    pub y: i32,
}

#[wasm_bindgen]
impl EllipticCurvePoint {
    #[wasm_bindgen(constructor)]
    pub fn new(x: i32, y: i32) -> EllipticCurvePoint {
        let point = EllipticCurvePoint { x: x, y: y };
        point
    }

    pub fn equals(&self, point: EllipticCurvePoint) -> bool {
        return self.x == point.x && self.y == point.y;
    }
}
