// import * as gpujs from 'gpu.js'

function helloTypescript (): string {
  console.log('Hello World from TypeScript!')
  return 'Hello TypeScript!!'
}

class GpuJsProvider {
  constructor () {
    // const gpu = new gpujs.GPU()
  }
}

export {
  helloTypescript,
  GpuJsProvider
}
