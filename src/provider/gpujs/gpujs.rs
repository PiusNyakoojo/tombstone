

use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "../src/provider/gpujs/gpujs")]
extern "C" {
    pub fn helloTypescript() -> String;
}
