
use usecase::elliptic_curve_usecase::EllipticCurveUsecase;

use wasm_bindgen::prelude::*;

use interface::js_api::JsApi;
use interface::js_api::elliptic_curve_api::EllipticCurveApi;

#[wasm_bindgen]
pub struct JsLibrary {
    #[allow(dead_code)]
    pub js_api: JsApi
}

#[wasm_bindgen]
impl JsLibrary {
    #[wasm_bindgen(constructor)]
    pub fn new() -> JsLibrary {
        let elliptic_curve_usecase = EllipticCurveUsecase::new();
        let elliptic_curve_api = EllipticCurveApi::new(elliptic_curve_usecase);
        let js_api = JsApi::new(elliptic_curve_api);
        JsLibrary { js_api: js_api }
    }
}