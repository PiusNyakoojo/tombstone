const TombStoneWasm = import('../../pkg/tombstone')

let TombStone

main()

async function main () {
  await init()
}

async function init () {
  TombStone = await TombStoneWasm.catch(console.error)
  const tombstoneLibrary = new TombStone.JsLibrary()
  const tombstoneApi = tombstoneLibrary.js_api
  const tombstoneCurveApi = tombstoneApi.elliptic_curve_api
  const curve = tombstoneCurveApi.create_curve()
  const point1 = tombstoneCurveApi.create_point(5, 1)
  const point2 = tombstoneCurveApi.create_point(5, 1)
  const sumPoint = tombstoneCurveApi.add_points(curve, point1, point2)
  console.log('Curve: ', curve)
  console.log('Points: ', point1, '; ', point2)
  console.log('Addition: ', sumPoint)
  console.log('Addition: ', 'x: ', sumPoint.x, ', y: ', sumPoint.y)

  tombstoneLibrary.hello()
}
