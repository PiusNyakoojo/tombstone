const rust = import('../../pkg/tombstone')

main()

async function main () {
  console.log('Hello')
  console.log('Rust: ', rust)
  const tombstone = await rust.catch(console.error)

  console.log('Tombstone: ', tombstone)

  const jsLibrary = new tombstone.JsLibrary()
  const jsApi = jsLibrary.js_api
  const curveApi = jsApi.elliptic_curve_api
  const curve = curveApi.create_curve()

  console.log('Js Library instance: ', jsLibrary)
  console.log('Curve API ', jsApi)
  console.log('Curve API ', curveApi)
  console.log('Curve ', curve.a_coefficient)

  // const todo = new tombstone.Todo()
  // const todoID = tombstone.get_todo_id(todo)

  // console.log('Todo instance: ', todo)
  // console.log('Todo ID: ', todoID)

  // tombstone.create_curve()

  // const api = new tombstone.JsLibApiWrapper()

  // console.log('Api: ', api)
  // console.log('JsApi: ', api.js_api)


  // tombstone.init_api()
  // tombstone.print_random_number()
  // console.log(tombstone.calculate_gcd(10, 5))
  console.log('Goodbye')
}
