# ⚰️💨 Tombstone

WebGL-accelerated elliptic curve cryptography compiled to WebAssembly. As fast as the speed of silence and as silent as a tombstone. Welcome to the crypt.

## Emoji

Yes, that is a coffin emoji. The tombstone is planned to be released [later](https://emojipedia.org/headstone/).

## License
MIT
